#%% imports
from pandas_ods_reader import read_ods
from pathlib import Path
import numpy as np
import itertools
import pandas as pd

#%% set stuff
f_name = Path('data', 'eeg_pc_run01.ods')
min_diff = 900

#%% load data
df = read_ods(f_name, 1)
df = df[['Mic 1',
         'Opto 1',
         'TTL in 1',
         'Elapsed mS'
         ]]

df['elapsed'] = df['Elapsed mS']

df['sound_onset'] = df['Mic 1'].diff()
df['trigger_onset'] = df['TTL in 1'].diff()
df['visual_onset'] = df['Opto 1'].diff()

#%% get all times of triggers
onsets = {
    'ttl': df.query('trigger_onset==1')['Elapsed mS'].values,
    'sound': df.query('sound_onset==1')['Elapsed mS'].values,
    'visual': df.query('visual_onset==1')['Elapsed mS'].values
}

#%% get onset differences within each modality and filter out those that are bogus
onset_diffs = {key: np.diff(val) for key, val in onsets.items()}

for cur_mod in onset_diffs.keys():
    done = False

    while not done:
        bad_idx = np.where(onset_diffs[cur_mod] < min_diff)[0]
        if bad_idx.size > 0:
            onsets[cur_mod] = np.delete(onsets[cur_mod], bad_idx[0]+1)
            onset_diffs[cur_mod] = np.diff(onsets[cur_mod])
            done = False
        else:
            done = True


#%% get delay lists
delays = {}
for cur_combination in itertools.combinations(onsets.keys(), 2):
    cur_key = '%s - %s' % (cur_combination[0], cur_combination[1])
    delays[cur_key] = onsets[cur_combination[0]] - onsets[cur_combination[1]]

delays_df = pd.DataFrame.from_dict(delays)