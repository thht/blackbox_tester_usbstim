# Data and analysis scripts for the preprint on the o_ptb

This is the data and the source code for the analysis of the timing accuracy
analysis reported in the [preprint covering the o_ptb]([https://psyarxiv.com/g4nbx/])

The source code of the paradigm as well as the specifications of the systems
used in this test can be found here: [https://gitlab.com/thht_experiments/blackbox_tester/-/tree/article]

## What's included?

The `data` folder contains the original and converted data from the two systems.

The `environment.yml` file can be used with `conda env create` to create the
python environment needed for the analysis.

`s01_analysis_bbtk.py` contains the actual analysis code.
